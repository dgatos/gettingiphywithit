//
//  GIFListViewController.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/4/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import UIKit
import ReSwift

class GIFListViewController: UIViewController, StoreSubscriber {
    typealias StoreSubscriberStateType = AppState

    let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    let searchBar = UISearchBar()
    //TODO: Localize Strings
    let segmentedControl = UISegmentedControl(items: ["Trending", "Search"])
    let tableView = UITableView(frame: CGRect.zero, style: .plain)
    var gifs: [GIFCellViewModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(GIFTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.backgroundColor = .black
        self.view.addSubview(tableView)
        tableView.frame = CGRect(x: 0, y: 64, width: self.view.frame.width, height: self.view.frame.height)

        self.view.addSubview(segmentedControl)
        segmentedControl.center = CGPoint(x: self.view.frame.width / 2, y: 40)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(action(sender:)), for: .valueChanged)

        //TODO: clear search bar sides
        //TODO: search bar hide when scrolling
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
        searchBar.tintColor = .clear
        searchBar.isHidden = true
        self.view.addSubview(searchBar)
        searchBar.frame = CGRect(x: 0, y: 56, width: self.view.frame.width, height: 36)

        self.view.addSubview(indicator)
        indicator.center = self.view.center

        store.subscribe(self)
        store.dispatch(SetType(type: .trending))
        store.dispatch(fetchGifs)
    }

    @objc func action(sender: UISegmentedControl) {
        if self.segmentedControl.selectedSegmentIndex == 0 {
            store.dispatch(SetType(type: .trending))
            store.dispatch(fetchGifs)
        } else {
            store.dispatch(SetType(type: .search(q: "")))
        }
    }

    /**
     UI as a function of current state
    **/
    func newState(state: AppState) {
        DispatchQueue.main.async {
            let loadingState = state.gifLoadingState

            if case .search = state.mainViewType {
                self.searchBar.isHidden = false
            } else if case .trending = state.mainViewType {
                self.searchBar.resignFirstResponder()
                self.searchBar.isHidden = true
            }

            self.indicator.stopAnimating()

            if case .loaded = loadingState {
                self.gifs = store.state.gifViewModels
                self.tableView.reloadData()
            } else if case .loading = loadingState {
                self.indicator.startAnimating()
            }
        }
    }
}

/**
 UITableViewDelegate and UITableViewDatasource
**/
extension GIFListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gifs.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let originalWidth = self.gifs[indexPath.row].width
        let originalHeight = self.gifs[indexPath.row].height
        return self.view.frame.width * originalHeight / originalWidth
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? GIFTableViewCell {
            cell.populate(viewModel: self.gifs[indexPath.row])

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //cancel gif download operations
        if let gifCell = cell as? GIFTableViewCell {
            gifCell.cancelDownload()
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
        let buffer: CGFloat = 40.0
        let scrollPosition = scrollView.contentOffset.y

        guard bottom > 0 else {
            return
        }

        //dismiss keyboard if its up
        if searchBar.isFirstResponder {
            searchBar.resignFirstResponder()
        }

        //load more
        if scrollPosition > bottom - buffer {
            if self.gifs.count == store.state.nextOffset {
                if case .loaded = store.state.gifLoadingState {
                    store.dispatch(fetchGifs)
                }
            }
        }
    }
}

/**
 UISearchBarDelegate: Handle Search Query
 **/
extension GIFListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let q = searchBar.text {
            store.dispatch(SetType(type: .search(q: q)))
            store.dispatch(fetchGifs)
            searchBar.resignFirstResponder()
        }
    }
}
