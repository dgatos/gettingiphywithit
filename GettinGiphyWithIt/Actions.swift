//
//  Actions.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/5/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import ReSwift

struct LoadMoreGIFs: Action {
    let offset: Int
}

struct SetGIFs: Action {
    let gifResult: GIFResult
}

struct SetType: Action {
    let type: MainViewType
}

