//
//  GIFTableViewCell.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/5/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import UIKit
import SwiftyGif

class GIFTableViewCell: UITableViewCell {
    
    var downloadGIFRequestToken: RequestToken?
    var gifView = UIImageView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.selectionStyle = .none
        
        self.contentView.addSubview(gifView)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func populate(viewModel: GIFCellViewModel) {
        self.gifView.backgroundColor = .yellow

        self.downloadGIFRequestToken = downloadGIF(url: viewModel.url, completion: { [weak self] (url) in
            guard let welf = self else { return }

            if let url = url, let image = try? UIImage(gifData: Data(contentsOf: url)) {
                DispatchQueue.main.async {
                    welf.gifView.setGifImage(image)
                }
            }
        })
    }

    override func layoutSubviews() {
        self.gifView.frame = self.contentView.bounds
    }

    func cancelDownload() {
        if self.imageView?.image == nil {
            self.downloadGIFRequestToken?.cancel()
        }
    }
}
