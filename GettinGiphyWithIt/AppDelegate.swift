//
//  AppDelegate.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/4/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import UIKit
import ReSwift

/***
ReSwift Store
***/
var store = Store(
    reducer: appReducer,
    state: AppState(),
    middleware: [])

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        if let window = window {
            window.backgroundColor = UIColor.white
            window.rootViewController = GIFListViewController()
            window.makeKeyAndVisible()
        }

        return true
    }
}

