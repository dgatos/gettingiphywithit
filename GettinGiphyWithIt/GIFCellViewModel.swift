//
//  GIFCellViewModel.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/5/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import UIKit

struct GIFCellViewModel {
    let width: CGFloat
    let height: CGFloat
    let url: URL
    let title: String

    init(gif: GIF) {
        self.width = CGFloat(gif.fixedWithImage.width)
        self.height = CGFloat(gif.fixedWithImage.height)
        self.url = gif.fixedWithImage.url
        self.title = gif.title
    }
}
