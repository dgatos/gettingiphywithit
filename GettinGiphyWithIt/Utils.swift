//
//  Utils.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/5/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import Foundation

class RequestToken {
    private weak var task: URLSessionDownloadTask?

    init() {}

    init(task: URLSessionDownloadTask) {
        self.task = task
    }

    func cancel() {
        task?.cancel()
    }
}
