//
//  MainStore.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/5/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import ReSwift

enum MainViewType {
    case trending
    case search(q: String)
}

enum GIFLoadingState {
    case idle
    case loading(offset: Int)
    case loaded
}

struct AppState: StateType {
    var mainViewType: MainViewType = .trending
    var gifLoadingState: GIFLoadingState = .idle
    var gifViewModels: [GIFCellViewModel] = []
    var nextOffset = 0
}
    
//TODO: Refactor next offset calculation
func appReducer(action: Action, state: AppState?) -> AppState {
    let mainViewType = state?.mainViewType ?? .trending
    let viewModels = state?.gifViewModels ?? []

    switch action {
    case let action as SetType:
        return AppState(mainViewType: action.type,
                        gifLoadingState: .loading(offset: 0),
                        gifViewModels: [],
                        nextOffset: 0)
    case let action as SetGIFs:
        return AppState(mainViewType: mainViewType,
                        gifLoadingState: .loaded,
                        gifViewModels: viewModels + action.gifResult.gifs.map { GIFCellViewModel(gif: $0) },
                        nextOffset: action.gifResult.pagination.count + action.gifResult.pagination.offset)
    case let action as LoadMoreGIFs:
        return AppState(mainViewType: mainViewType,
                        gifLoadingState: .loading(offset: action.offset),
                        gifViewModels: viewModels,
                        nextOffset: action.offset + 25)
    default:
        return AppState(mainViewType: mainViewType,
                        gifLoadingState: .idle,
                        gifViewModels: viewModels,
                        nextOffset: 0)
    }
}
