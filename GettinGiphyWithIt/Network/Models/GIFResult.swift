//
//  GIFResult.swift
//  GIFResult
//
//  Created by Daniel Katz on 11/4/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import Foundation

struct Pagination: Decodable {
    let totalCount: Int?
    let count: Int
    let offset: Int

    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case count
        case offset
    }
}

struct GIFResult: Decodable {
    let gifs: [GIF]
    let pagination: Pagination

    enum CodingKeys: String, CodingKey {
        case gifs = "data"
        case pagination
    }
}
