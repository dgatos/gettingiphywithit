//
//  Gif.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/4/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import Foundation


struct GIFImage: Codable {
    let url: URL
    var width: Int = 0
    var height: Int = 0

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decode(URL.self, forKey: CodingKeys.url)
        
        if let width = Int(try container.decode(String.self, forKey: CodingKeys.width)) {
            self.width = width
        }
        if let height = Int(try container.decode(String.self, forKey: CodingKeys.height)) {
            self.height = height
        }
    }
}

struct GIF: Decodable {
    let type: String
    let url: String
    let title: String
    let fixedWithImage: GIFImage

    enum CodingKeys: String, CodingKey {
        case type
        case url
        case title
        case images
    }

    enum ImageCodingKeys: String, CodingKey {
        case fixedWidth = "fixed_width"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decode(String.self, forKey: .type)
        self.url = try container.decode(String.self, forKey: .url)
        self.title = try container.decode(String.self, forKey: .title)

        let images = try container.nestedContainer(keyedBy: ImageCodingKeys.self, forKey: .images)
        self.fixedWithImage = try images.decode(GIFImage.self, forKey: .fixedWidth)
    }
}
