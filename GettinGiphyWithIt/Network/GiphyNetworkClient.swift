//
//  File.swift
//  GettinGiphyWithIt
//
//  Created by Daniel Katz on 11/5/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import Foundation
import ReSwift

//TODO: More robust network handling decoupled from ReSwift
let apiKey = "rE156PcdxtgHynu0nRm7zjw8LLTpPVUF"
let baseURLString = "https://api.giphy.com"

func fetchGifs(state: AppState, store: Store<AppState>) -> Action? {
    let viewType = state.mainViewType
    let nextOffset = state.nextOffset
    let urlString = "\(baseURLString)\(viewType.giphyURLPath)?api_key=\(apiKey)&\(viewType.defaultParams)&offset=\(nextOffset)"
    let url = URL(string: urlString)!

    URLSession.shared.dataTask(with: url) { (data, response, error) in
        if error != nil {
            print(error!.localizedDescription)
        }

        guard let data = data else { return }
        do {
            let gifResult = try JSONDecoder().decode(GIFResult.self, from: data)
            store.dispatch(SetGIFs(gifResult: gifResult))
        } catch let jsonError {
            //TODO: proper logging
            print(jsonError)
        }
    }.resume()

    return LoadMoreGIFs(offset: nextOffset)
}

func downloadGIF(url: URL, completion: @escaping (URL?) -> Void) -> RequestToken {
    let escapedURL = url.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let destinationUrl = URL(fileURLWithPath: NSTemporaryDirectory())
        .appendingPathComponent(escapedURL)

    if FileManager.default.fileExists(atPath: destinationUrl.path) {
        completion(destinationUrl)
        return RequestToken()
    }

    let task = URLSession.shared.downloadTask(with: url) { (url, response, error) in
        if let url = url {
            do {
                try FileManager.default.moveItem(at: url, to: destinationUrl)
                completion(destinationUrl)
            } catch {
                print("move file failed")
                completion(nil)
            }
        }
    }

    task.resume()

    return RequestToken(task: task)
}

extension MainViewType {
    var giphyURLPath: String {
        switch self {
        case .search:
            return "/v1/gifs/search"
        case .trending:
            return "/v1/gifs/trending"
        }
    }

    var defaultParams: String {
        switch self {
        case .search(let q):
          let escapedQuery = q.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
          return "limit=25&rating=G&q=\(escapedQuery)"
        case .trending:
          return "limit=25&rating=G"
        }
    }
}
