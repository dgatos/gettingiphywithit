//
//  JSONTests.swift
//  JSONTests
//
//  Created by Daniel Katz on 11/4/17.
//  Copyright © 2017 Daniel Katz. All rights reserved.
//

import XCTest
@testable import GettinGiphyWithIt

class JSONTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testTrendingResultsJSONParsing() {
        let jsonData = self.jsonData(forResource: "trending")
        let decoder = JSONDecoder()
        var result: GIFResult

        do {
            result = try decoder.decode(GIFResult.self, from: jsonData)
        } catch let error {
            fatalError("Unable to convert trending.json to Gif result \(error)")
        }

        XCTAssert(result.gifs.count == 1)
        XCTAssert(result.gifs.first!.title == "weekend saturday GIF by Pukkelpop")
        XCTAssert(result.gifs.first!.fixedWithImage.url.absoluteString == "https://media3.giphy.com/media/lAK7ACXBaEy4/200w.gif")
        XCTAssert(result.gifs.first!.fixedWithImage.width == 200)
        XCTAssert(result.pagination.totalCount == nil)
        XCTAssert(result.pagination.count == 1)
    }

    func testSearchResultsJSONParsing() {
        let jsonData = self.jsonData(forResource: "search")
        let decoder = JSONDecoder()
        var result: GIFResult

        do {
           result = try decoder.decode(GIFResult.self, from: jsonData)
        } catch let error {
            fatalError("Unable to convert search.json to Gif result \(error)")
        }

        XCTAssert(result.gifs.count == 1)
        XCTAssert(result.gifs.first!.title == "happy the fresh prince of bel air GIF")
        XCTAssert(result.pagination.count == 1)
        XCTAssert(result.pagination.totalCount == 25722)
    }

    func jsonData(forResource resource: String) -> Data {
        guard let pathString = Bundle(for: type(of: self)).path(forResource: resource, ofType: "json") else {
            fatalError("\(resource).json not found")
        }

        guard let jsonString = try? NSString(contentsOfFile: pathString, encoding: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(resource).json to String")
        }

        print("The JSON string is: \(jsonString)")

        guard let jsonData = jsonString.data(using: String.Encoding.utf8.rawValue) else {
            fatalError("Unable to convert \(resource).json to NSData")
        }

        return jsonData
    }
}
