# Gettin Giphy With It

#### Demo
1. [Video](https://bitbucket.org/dgatos/gettingiphywithit/raw/master/demo.mov)

#### Project Setup

1. Install Carthage [Download](https://github.com/Carthage/Carthage)
1. Run `carthage bootstrap`
1. Open GettinGiphyWithIt.xcodeproj

#### Project Highlights
1. Unidirectional data flow using ReSwift
1. Infinite Scrolling
1. Cancellable network requests (fast scrolling)
1. JSON parsing (Swift 4 Decodable) + Tests

#### Additional Questions
1. Time spent - 7.5 hours
1. Most difficult - Load more as scrolling with data flow
1. Technical Debt - Linting, Logging, Key externalization, More robust networking, Autolayout, Better Caching (search TODO:)
